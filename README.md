# Slush React project scaffold

### Install and use

Install slush

```bash
npm install -g slush
```

Install generator

```bash
npm install -g git+ssh://git@bitbucket.org/frontendmafia/slush-react.git
```

Scaffold a basic React project.

```bash
mkdir my-project-folder
cd my-project-folder
slush react
```

Run development server

```bash
npm start
```