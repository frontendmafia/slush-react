var gulp = require('gulp'),
	install = require('gulp-install'),
	conflict = require('gulp-conflict'),
	template = require('gulp-template'),
	inquirer = require('inquirer');

var isWin = /^win/.test(process.platform);

gulp.task('default', function ( done ) {

	inquirer.prompt([{
		type: 'input',
		name: 'appName',
		message: 'Name for the app? (no spaces or special characters)',
		'default': ( isWin ) ? __dirname.split('\\\\').pop() : __dirname.split('/').pop()
	}], function ( answers ) {
		console.log('answers', answers);
		gulp.src(__dirname + '/app/**') // Relative to __dirname
			.pipe(template(answers))
			.pipe(conflict('./'))
			.pipe(gulp.dest('./')) // Relative to cwd
			.pipe(install())
			.on('finish', function () {
				done(); // Finished!
			});
	});

});
