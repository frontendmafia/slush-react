var gulp = require('gulp'),
	plumber = require('gulp-plumber'),
	gulpWebpack = require('gulp-webpack'),
	webpack = require('webpack');

gulp.task('webpack', function () {
	gulp.src('./src/scripts/main.js')
		.pipe(plumber())
		.pipe(gulpWebpack(require('./../webpack.config.js')))
		.pipe(gulp.dest('./dist'));
});
