var gulp = require('gulp');

gulp.task('watch', function () {

	gulp.watch([
		'./src/scripts/**/*.js',
		'./src/scripts/**/*.jsx'
	], ['webpack']);

	gulp.watch('./src/index.html', ['html'])

});
