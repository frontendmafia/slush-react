/** @jsx React.DOM */
var React = require('react');

/**
 * App
 */
var App =  React.createClass({

	getInitialState: function () {
		return {}
	},

	render: function () {
		return (
			<div className="App">App</div>
		);
	}
});

module.exports = App;

//class App extends React.Component {
//	constructor ( props ) {
//		super(props);
//		this.state = { count: props.initialCount };
//	}
//	render () {
//		return (
//			<div>Hello React</div>
//		);
//	}
//}
