var path = require('path');
var webpack = require('webpack');

module.exports = {
	entry: './src/scripts/main.js',
	output: {
		path: __dirname + '/dist',
		filename: '[name].bundle.js',
		publicPath: './dist'
	},
	resolve: {
		extensions: ['', '.js', '.jsx', '.less'],
		root: [
			__dirname + '/src/scripts/'
		]
	},
	watch: false,
	module: {
		loaders: [
			//{ test: /\.(js|jsx)$/, exclude: /node_modules/, loader: '6to5-loader?experimental&optional=selfContained!jsx-loader' },
			{ test: /\.(js|jsx)$/, exclude: /node_modules/, loader: '6to5-loader!jsx-loader?harmony&insertPragma=React.DOM' },
			{ test: /\.less$/, loader: 'style-loader!css-loader!less-loader' },
			{ test: /\.css$/, loader: 'style-loader!css-loader' }
		]
	},
	plugins: [
		//new webpack.optimize.UglifyJsPlugin(),
		//new webpack.optimize.CommonsChunkPlugin('common', 'common.bundle.js', ['./bootstrap.js'], 5),
		//new webpack.DefinePlugin({
		//	VERSION: JSON.stringify(require('./package.json').version)
		//})
	]
};
